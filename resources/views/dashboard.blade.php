@extends('layouts.app')

@section('content')

@include('layouts.navbar')

    <div class="row m-4">
    <div class="col-xl-4 col-lg-6">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Total de CPF`S</h5>
              <span class="h2 font-weight-bold mb-0">{{ $formCount }}</span>
              <input type="hidden" id="formSeven" value="{{ $formSeven }}">
              <input type="hidden" id="formFourteen" value="{{ $formFourteen }}">
            </div>

            <div class="col-auto">
                <i class="far fa-copy"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
        <div class="col-6">
            <div id="chart_div"></div>
        </div>
    </div>




    @livewire('form-table')

@endsection
