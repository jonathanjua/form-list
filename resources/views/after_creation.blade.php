@extends('layouts.app')

@section('content')
            <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="https://www.clipartmax.com/png/full/10-104506_form-icon-orcamento-icon.png" alt="" width="72">
                <h2>Lorem Ipsum</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt, odio nec ultrices dictum, turpis orci condimentum lectus, a sagittis elit massa vel purus. Quisque ut nunc ante.</p>
              </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                        <strong>{{ $message }}</strong>
                </div>

                <a class="btn btn-primary position-absolute top-50 start-50 translate-middle" href="{{ route('index') }}" role="button">Clique aqui para preencher o formulário novamente</a>

            @endif

            @if ($message = Session::get('warning'))

            <div class="alert alert-warning alert-block">
                    <strong>{{ $message }}</strong>
            </div>

            <a class="btn btn-primary " href="{{ route('thanks') }}" role="button">Manter Formulario</a>

            <a class="btn btn-primary " href="{{ route('afterApproval') }}" role="button">Atualizar Formulario</a>

            <a class="btn btn-primary " href="{{ route('index') }}" role="button">Preencher o formulário novamente</a>

            @endif
@endsection
