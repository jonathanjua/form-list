<div>
    {{-- {{ $state}}
    {{ $date_end }}
    {{ $date_start }} --}}
<div class="row m-4">
    <div class="col">
        <label class="control-label" for="date">Status</label>
        <select class="form-select" aria-label="status" name="state" wire:model="state">
            <option value="all" >Todos</option>
            <option value="waiting" >Aguardando</option>
            <option value="approve">Aprovado</option>
            <option value="filed">Arquivado</option>
        </select>
    </div>
    <div class="col">
        <label class="control-label" for="date">Date Inicial</label>
        <input wire:model="date_start" class="form-control" id="date" name="date_start" placeholder="MM/DD/YYY" type="date"/>
    </div>

    <div class="col">
        <label class="control-label" for="date">Date Final</label>
        <input  wire:model="date_end"  class="form-control" id="date" name="date_end" placeholder="MM/DD/YYY" type="date"/>
    </div>

    <div class="col">
        <button wire:click="filter" class="btn btn-primary mt-4" name="submit" type="submit">Filtra</button>
    </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                        <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block">
                    <strong>{{ $message }}</strong>
            </div>
            @endif

        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">CPF</th>
                <th scope="col">Data Envio</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>

        @forelse ($forms as $form)
            <tr >
            <th scope="row">{{ $form->name}}</th>
            <td> {{ $form->email }} </td>
            <td> {{ $form->cpf }} </td>
            <td> {{ date('d/m/Y H:i:s', strtotime($form->created_at)); }}</td>
            <td>
                {{ $form->state_type == "waiting"?'Aguardando':'' }}
                {{ $form->state_type == "approve"?'Aprovado':'' }}
                {{ $form->state_type == 'filed'?'Arquivado':'' }}
            </td>
            <td>

            @if($form->state_type == 'waiting')
            <a wire:click="approve({{ $form->id }})" class="btn btn-success btn-sm ">Aprovar</a>
            @endif

            @if($form->state_type != 'filed')
            <a wire:click="filed({{ $form->id }})" class="btn btn-warning btn-sm m-1" role="button" >Arquivar</a>
            @endif

            <a class="btn btn-primary btn-sm" role="button" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $form->id }}" >Visualizar</a>
            </td>
            <td>
             <button wire:click="delete({{ $form->id }})" class="btn btn-danger btn-sm">Delete</button>
            </td>
            </tr>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal{{ $form->id }}" tabindex="-1" aria-labelledby="exampleModalLabel{{ $form->id }}" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ $form->name }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Email: {{ $form->email }} <br>
                        CPF: {{ $form->cpf }} <br>
                        RG: {{ $form->rg }} <br>
                        CEP: {{ $form->cep }} <br>
                        Endereço: {{ $form->street }}, {{ $form->district }}, {{ $form->number }}, {{ $form->city }} - {{ $form->state }}<br>
                        Complemento: {{ $form->complement }} <br>
                        Data de Envio: {{ date('d/m/Y', strtotime($form->created_at)); }}<br>
                        <br>
                        imagem CPF:
                        <img src="{{ asset('/img/cpf/'.$form->id.'.jpg')}}" class="img-thumbnail mt-4" > <br>
                        <br>
                        imagem RG:
                        <img src="{{ asset('/img/rg/'.$form->id.'.jpg')}}" class="img-thumbnail mt-4" >

                    </div>

                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>

                    </div>
                </div>
                </div>

            </div>

        @endforeach

           </tbody>
          </table>

</div>
