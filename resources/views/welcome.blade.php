@extends('layouts.app')

@section('content')
            <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="https://www.clipartmax.com/png/full/10-104506_form-icon-orcamento-icon.png" alt="" width="72">
                <h2>Lorem Ipsum</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt, odio nec ultrices dictum, turpis orci condimentum lectus, a sagittis elit massa vel purus. Quisque ut nunc ante.</p>
              </div>

            <form class="row g-3" action="{{ route('create') }}" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="col-md-6">
                    <label  class="form-label">Nome</label>
                    <input type="text" class="form-control" id="name" name="name"  value="{{ old('name') }}" required>
                    @if ($errors->has('name'))
                     <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="col-md-6">
                  <label for="inputEmail4" class="form-label">Email</label>
                  <input type="email" class="form-control" id="email" name="email"  value="{{ old('email') }}" required>
                  @if ($errors->has('email'))
                     <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="col-md-6">
                    <label  class="form-label">CPF</label>
                    <input type="text" class="form-control" name="cpf" id="cpf" placeholder="000.000.000-00" maxlength="14" OnBlur="validCPF();" value="{{ old('cpf') }}" required>
                    @if ($errors->has('cpf'))
                     <div class="alert alert-danger">{{ $errors->first('cpf') }}</div>
                    @endif
                </div>
                <div class="col-md-6">
                    <label  class="form-label">RG</label>
                    <input type="text" class="form-control" id="rg" name="rg"  value="{{ old('rg') }}" required>
                    @if ($errors->has('rg'))
                     <div class="alert alert-danger">{{ $errors->first('rg') }}</div>
                    @endif
                </div>
                <div class="col-md-3">
                    <label  class="form-label">Cep</label>
                    <input type="text" class="form-control cep" id="cep" name="cep" value="{{ old('cep') }}" required>
                    @if ($errors->has('cep'))
                     <div class="alert alert-danger">{{ $errors->first('cep') }}</div>
                    @endif
                </div>
                <div class="col-md-6">
                    <label  class="form-label">Rua</label>
                    <input type="text" class="form-control" id="street" name="street" value="{{ old('street') }}" required>
                    @if ($errors->has('street'))
                     <div class="alert alert-danger">{{ $errors->first('street') }}</div>
                    @endif
                </div>
                <div class="col-md-3">
                    <label  class="form-label">Número</label>
                    <input type="text" class="form-control" id="number" name="number" value="{{ old('number') }}" required>
                    @if ($errors->has('number'))
                     <div class="alert alert-danger">{{ $errors->first('number') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <label  class="form-label">Bairro</label>
                    <input type="text" class="form-control" id="district" name="district"  value="{{ old('district') }}" required>
                    @if ($errors->has('district'))
                     <div class="alert alert-danger">{{ $errors->first('district') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <label  class="form-label">Cidade</label>
                    <input type="text" class="form-control" id="city" name="city"  value="{{ old('city') }}" required>
                    @if ($errors->has('city'))
                    <div class="alert alert-danger">{{ $errors->first('city') }}</div>
                   @endif
                </div>
                <div class="col-md-4">
                    <label  class="form-label">Estado</label>
                    <input type="text" class="form-control" id="state" name="state"  value="{{ old('state') }}" required>
                    @if ($errors->has('state'))
                    <div class="alert alert-danger">{{ $errors->first('state') }}</div>
                    @endif
                </div>
                <div class="col-md-12">
                    <label  class="form-label">Complemento</label>
                    <input type="text" class="form-control" id="complement" name="complement" value="{{ old('complement') }}" required>
                    @if ($errors->has('complement'))
                    <div class="alert alert-danger">{{ $errors->first('complement') }}</div>
                   @endif
                </div>
                <div class="input-group mb-3">
                    <input type="file" class="form-control" name="cpf_img" value="{{ old('cpf_img') }}" required>
                    <label class="input-group-text">Imagem do CPF</label>
                </div>
                @if ($errors->has('cpf_img'))
                    <div class="alert alert-danger">{{ $errors->first('cpf_img') }}</div>
                   @endif
                <div class="input-group mb-3">
                    <input type="file" class="form-control" name="rg_img"  value="{{ old('rg_img') }}" required>
                    <label class="input-group-text">Imagem do RG</label>
                </div>
                @if ($errors->has('rg_img'))
                    <div class="alert alert-danger">{{ $errors->first('rg_img') }}</div>
                   @endif
                <div class="col-md-12">
                    <label  class="form-label">Descrição dos Serviços.</label >
                    <textarea class="form-control" id="description" name="description" cols="30" rows="10" {{ old('description') }} required>{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                    <div class="alert alert-danger">{{ $errors->first('description') }}</div>
                   @endif
                   <p><span id="charactersRemaining" style="font-weight: bold;">500</span> caracteres restantes</p>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Google Recaptcha</label>
                    <div class="col-md-6"> {!! htmlFormSnippet() !!} </div>
                    @if ($errors->has('g-recaptcha-response'))
                    <div class="alert alert-danger">{{ $errors->first('g-recaptcha-response') }}</div>
                   @endif
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary">ENVIAR</button>
                  </div>
              </form>
        </div>

@endsection
