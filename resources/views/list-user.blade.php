@extends('layouts.app')

@section('content')

@include('layouts.navbar')

<a class="btn btn-primary m-4" href="{{ route('add-user') }}" role="button">Adicionar Usuario</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nome</th>
        <th scope="col">Email</th>
      </tr>
    </thead>
    <tbody>
    @foreach ( $users as $user )
      <tr>
        <th scope="row">1</th>
        <td> {{$user->name}}</td>
        <td> {{$user->email}}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
