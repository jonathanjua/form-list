@extends('layouts.app')

@section('content')

@include('layouts.navbar')



<form class="row g-3" action="{{ route('user-create') }}" method="POST">
    @csrf
    <div class="col-md-12">
        <label  class="form-label">Nome</label>
        <input type="text" class="form-control" id="name" name="name"  value="{{ old('name') }}">
        @if ($errors->has('name'))
         <div class="alert alert-danger">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <div class="col-md-12">
      <label for="inputEmail4" class="form-label">Email</label>
      <input type="email" class="form-control" id="email" name="email"  value="{{ old('email') }}">
      @if ($errors->has('email'))
         <div class="alert alert-danger">{{ $errors->first('email') }}</div>
        @endif
    </div>
    <div class="col-md-12">
        <label  class="form-label">Senha</label>
        <input type="password" class="form-control" id="password" name="password"  value="">
        @if ($errors->has('password'))
         <div class="alert alert-danger">{{ $errors->first('password') }}</div>
        @endif
    </div>
    <div class="col-12">
      <button type="submit" class="btn btn-primary">CRIAR</button>
    </div>
  </form>

@endsection
