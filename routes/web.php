<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [ FormController::class, 'index'])->name('index');
Route::post('/formsubmit', [ FormController::class, 'store'])->name('create');
Route::post('/form-delete', [ FormController::class, 'destroy'])->name('form-delete');
Route::get('/aftercreation', function () {
    return view('after_creation');
})->name('aftercreation');
Route::get('/afterApproval', [ FormController::class, 'afterApproval'])->name('afterApproval');

Route::get('/list-user', [DashboardController::class, 'listUser'])->name('list-user');

Route::get('/list-user', [DashboardController::class, 'listUser'])->name('list-user');
Route::get('/add-user', [UserController::class, 'index'])->name('add-user');
Route::post('/user-create', [UserController::class, 'store'])->name('user-create');

Route::get('/thanks', function () {
    return view('thanks');
})->name('thanks');
