<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:5',
            'email' => 'required|email',
            'cpf' => 'required',
            'rg' => 'required|numeric',
            'cep' => 'required|numeric',
            'street' => 'required',
            'number' => 'required',
            'district' => 'required',
            'complement' => 'required',
            'city' => 'required',
            'state' => 'required',
            'cpf_img'=> 'required|image|mimes:jpg|max:4096',
            'rg_img' => 'required|image|mimes:jpg|max:4096',
            'description' => 'required|max:500|min:5',
            'g-recaptcha-response' => 'recaptcha'
            ];
    }
}
