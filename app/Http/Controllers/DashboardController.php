<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $formCount = Form::count();

        $date = date("Y-m-d", strtotime('+1 days'));
        $sevenDays = date("Y-m-d", strtotime('-7 days'));
        $fourteenDays = date("Y-m-d", strtotime('-14 days'));

        $formSeven = Form::whereBetween('created_at', [$sevenDays, $date])->count();
        $formFourteen = Form::whereBetween('created_at', [$fourteenDays, $sevenDays])->count();

        

        return view('dashboard', compact('formCount', 'formSeven', 'formFourteen'));
    }


    public function listUser()
    {

    $users = User::all();
    return view('list-user', compact('users'));

   }
}
