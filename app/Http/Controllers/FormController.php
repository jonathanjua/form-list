<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitFormRequest;
use App\Models\Form;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {


        return view('welcome');
    }

    public function store(SubmitFormRequest $request)
    {
        $form = new Form();

        $form->name = $request->name;
        $form->email = $request->email;
        $form->cpf = $request->cpf;
        $form->rg = $request->rg;
        $form->cep = $request->cep;
        $form->street = $request->street;
        $form->number = $request->number;
        $form->district = $request->district;
        $form->complement = $request->complement;
        $form->city = $request->city;
        $form->state = $request->state;
        $form->description = $request->description;

        $checkEmail = Form::where('email', $request->email)->count();
        $checkcpf = Form::where('cpf', $request->cpf)->count();

        if($checkEmail or $checkcpf)
        {
            session()->put('form', $form);

            return redirect()->route('aftercreation')->with('warning','VOCÊ JA POSSUI UM FORMULARIO CADASTRADO');
        }

        $form->save();

        $url_img_cpf = $form->id.'.'.$request->cpf_img->extension();
        $request->cpf_img->move(public_path().'/img/cpf/', $url_img_cpf);

        $url_img_rg = $form->id.'.'.$request->rg_img->extension();
        $request->rg_img->move(public_path().'/img/rg/', $url_img_rg );

        return redirect()->route('aftercreation')->with('success','Formulario enviado com Sucesso!!');
    }

    public function destroy(Request $request)
    {
        $form = form::find($request->id);
        $form->delete();
        return redirect()->route('dashboard')
        ->with('success','Formulario excluido com Sucesso!!');
    }

    public function approve(){


    }

    public function afterApproval()
    {
        $formDate = session()->get('form');

        $form = Form::where('cpf', $formDate->cpf)->first();

        $form->name = $formDate->name;
        $form->email = $formDate->email;
        $form->cpf = $formDate->cpf;
        $form->rg = $formDate->rg;
        $form->cep = $formDate->cep;
        $form->street = $formDate->street;
        $form->number = $formDate->number;
        $form->district = $formDate->district;
        $form->complement = $formDate->complement;
        $form->city = $formDate->city;
        $form->state = $formDate->state;
        $form->description = $formDate->description;

        $form->save();

        session()->flush();
        return redirect()->route('thanks');
    }
}
