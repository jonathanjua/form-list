<?php

namespace App\Http\Livewire;

use App\Models\Form;
use Illuminate\Contracts\Session\Session;
use Livewire\Component;
use Livewire\WithPagination;

class FormTable extends Component
{
    use WithPagination;

    public $state;
    public $date_start;
    public $date_end;
    public $forms=[];

    protected $listeners = [
        'reviewSectionRefresh' => '$refresh',
    ];

    public function mount()
    {
        $this->date_start = date("2000-01-01");
        $this->date_end = date("Y-m-d", strtotime('+1 days'));
        $this->state = 'all';
        $this->forms = Form::all();
    }

    public function render()
    {

        return view('livewire.form-table');
    }

    public function filter()
    {

        if($this->state != 'all')
        {
            $this->forms = Form::where('state_type', $this->state)
            ->whereBetween('created_at', [$this->date_start, $this->date_end])->get();
        }

        if($this->state == 'all')
        {
           $this->forms = Form::whereBetween('created_at', [$this->date_start, $this->date_end])->get();
        }

    }

    public function delete($id){

        $form = Form::find($id);
        $form->delete();

        $this->emit('reviewSectionRefresh');

    }

    public function approve($id){

        $form = Form::find($id);
        $form->state_type = 'approve';
        $form->save();
        $this->emit('reviewSectionRefresh');
    }

    public function filed($id){

        $form = Form::find($id);
        $form->state_type = 'filed';
        $form->save();
        $this->emit('reviewSectionRefresh');
    }

}
