google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries() {

    var formSeven =  parseInt(document.getElementById("formSeven").value);
    var formFourteen =  parseInt(document.getElementById("formFourteen").value);

      var data = google.visualization.arrayToDataTable([
        ['Form', 'Formularios enviados'],

        ['Última Semana', formSeven],
        ['Semana Anterior ', formFourteen],
      ]);

      var options = {
        title: 'Envio de Formularios',
        chartArea: {width: '50%'},
        hAxis: {
          title: 'Total de Formularios',
          minValue: 0
        },
        vAxis: {
          title: ''
        }
      };

      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
