
/////// CEP VALIDATION

const clearForm = (address) => {
    document.getElementById('street').value = '';
    document.getElementById('complement').value = '';
    document.getElementById('district').value = '';
    document.getElementById('city').value = '';
    document.getElementById('state').value = '';
}

const fillForm = (address) => {
    document.getElementById('street').value = address.logradouro;
    document.getElementById('complement').value = address.complemento;
    document.getElementById('district').value = address.bairro;
    document.getElementById('city').value = address.localidade;
    document.getElementById('state').value = address.uf;
}

const isnumber = (number) => /^[0-9]+$/.test(number);

const cepvalid = (cep) => cep.length == 8 && isnumber(cep);

const searchCep = async() =>{

    clearForm();

    const cep = document.getElementById('cep').value;
    const url = `http://viacep.com.br/ws/${cep}/json/`;

    if(cepvalid(cep)){

        const data = await fetch(url);
        const address = await data.json();

        if(address.hasOwnProperty('erro')){
            alert("CEP não encontrado!");
        }else{
         fillForm(address);
        }
    }else{
        alert("CEP incorreto");
    }
}

document.getElementById('cep')
        .addEventListener('focusout', searchCep);

/////// CPF VALIDATION

function validCPF(){

    var cpf=document.getElementById("cpf").value ;
    var cpfvalid = /^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}))$/;

        if (cpfvalid.test(cpf) == false)    {

            cpf = cpf.replace( /\D/g , "");

            if (cpf.length==11){
                cpf = cpf.replace( /(\d{3})(\d)/ , "$1.$2");
                cpf = cpf.replace( /(\d{3})(\d)/ , "$1.$2");
                cpf = cpf.replace( /(\d{3})(\d{1,2})$/ , "$1-$2");

                var valueValid = document.getElementById("cpf").value = cpf;
            }else{
                alert("CPF invalido");
            }

        }
    }

/// TEXT AREA

var textarea = document.querySelector('textarea');
var info = document.getElementById('charactersRemaining');
var limit = 500;

textarea.addEventListener('keyup', toCheck);

function toCheck(e) {
    var theAmount = this.value.length;
    var remaining = limit - theAmount;
    if (remaining < 1) {
        this.value = this.value.slice(0, limit);
        return info.innerHTML = 0;
    }
    info.innerHTML = remaining;
}

